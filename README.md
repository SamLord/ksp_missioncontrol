# README #

### What is this repository for? ###

KSP_MissionControl is an mission editor/runner to be used in conjunction with the [krpc mod](https://github.com/krpc/krpc).

[Watch the demo](https://youtu.be/sw62fgbvyjY)

Pull requests and forks welcome.

### Setup ###

1. Download and install the [krpc server mod](https://github.com/krpc/krpc/releases) (NOT the client). And then get going using the krpc [documentation](https://krpc.github.io/krpc/getting-started.html)
 * Instructions on how to install a KSP mod can be found [here](https://forum.kerbalspaceprogram.com/index.php?/topic/110912-how-to-installing-mods/).
 
2. Download the latest version of [KSP_MissionControl](https://bitbucket.org/SamLord/ksp_missioncontrol/downloads/) (prebuilt version coming soon)

3. Build your first mission, or open a saved one. Launch KSP and get a rocket placed on the launch pad. Then just click Missions->Start Mission and you should see your mission play out!

### Latest binary download ###

* Current Version - v1.0.0.0
[Download](https://bitbucket.org/SamLord/ksp_missioncontrol/downloads/)

### Licence ###

Do what you like with the code in this repo. I've credited any code that I've cut/paste from elsewhere and that may have it's own licences. But anything else is yours to do what you want with. Credit linking back to here is much appreciated but not a requirement :)

### Mind blown? ##

Like what you see here or really want me to do some more work on this? Feel free to buy me a pint:

# [![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=W8N3N72TVT88L) #