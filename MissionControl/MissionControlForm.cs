﻿using MissionControl.Missions;
using MissionControl.Missions.Aborts;
using MissionControl.Missions.Aborts.Conditions;
using MissionControl.Missions.Events;
using MissionControl.Missions.Events.AutoPilotEvents;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using MissionControl.Extensions;
using MissionControl.Editor;

namespace MissionControl
{
    public partial class MissionControlForm : Form
    {
        MissionBuilder missionBuilder;

        Mission missionOne;
        Mission missionTwo;

        //List of all MissionEvent types
        List<Type> allEvents;

        public MissionControlForm()
        {
            InitializeComponent();
            missionBuilder = new MissionBuilder();

            //Populate events dropdown with all the events and resize as req.
            allEvents = System.Reflection.Assembly.GetExecutingAssembly().GetNonAbstractTypesInNamespace("MissionControl.Missions.Events");
            int maxWidth = 0;
            List<Type> removalList = new List<Type>();
            foreach (Type type in allEvents)
            {
                //separate out the name to compnent parts
                string[] typeNameFull = type.ToString().Split('.');
                //extract last element and go camelCase -> Real Words
                string name = System.Text.RegularExpressions.Regex.Replace(typeNameFull[typeNameFull.Length - 1], @"(\B[A-Z]+?(?=[A-Z][^A-Z])|\B[A-Z]+?(?=[^A-Z]))", " $1");
                if (!name.Contains("+")) //Stop the async Task "Activate" methods being included for some reason + stop child classes being included
                {
                    eventsComboBox.Items.Add(name);
                }
                else
                {
                    removalList.Add(type);
                }

                //Find the longest name
                int currentItemWidth = TextRenderer.MeasureText(name, eventsComboBox.Font).Width;
                if (currentItemWidth > maxWidth)
                {
                    maxWidth = currentItemWidth;
                }
            }
            eventsComboBox.Width = maxWidth; //resize to fit longest name
            addEventButton.Enabled = false;

            //Clear the unused types from the list of all types
            foreach (Type type in removalList)
            {
                allEvents.Remove(type);
            }
        }

        #region Missions
        private void GenerateMissionOne()
        {
            IPAddress connectionTarget = System.Net.IPAddress.Parse("127.0.0.1");

            #region Internal mission
            //Testing internal mission so we'll do some stuff in an internal mission - the conditional stuff more accurately
            List<MissionEvent> internalMissionEvents = new List<MissionEvent>();
            internalMissionEvents.Add(new EnsureAboveAltitudeEvent(19500));
            internalMissionEvents.Add(new EnableAutoPilotEvent(true));
            internalMissionEvents.Add(new TurnToHeadingEvent(90, 90));
            internalMissionEvents.Add(new GravityTurnEvent(20000, 50000, true));
            internalMissionEvents.Add(new EnableAutoPilotEvent(false));
            internalMissionEvents.Add(new SASChangeActiveEvent(true));

            Mission internalGravityTurnMission = new Mission(internalMissionEvents, missionOutputTextBox, connectionTarget);
            #endregion

            #region Abort
            List<Condition> conditions = new List<Condition>();
            conditions.Add(new UncontrolledFallingCondition(1200));
            List<MissionEvent> abortEvents = new List<MissionEvent>();
            abortEvents.Add(new NextStageEvent());
            abortEvents.Add(new NextStageEvent());
            abortEvents.Add(new NextStageEvent());
            abortEvents.Add(new NextStageEvent());
            abortEvents.Add(new NextStageEvent());
            abortEvents.Add(new ActivateParachutesEvent());

            List<Abort> aborts = new List<Abort>();
            aborts.Add(new Abort(conditions, abortEvents));
            #endregion

            List<MissionEvent> parentMissionEvents = new List<MissionEvent>();

            //PREP
            parentMissionEvents.Add(new AnnouncementEvent("Readying equipment and preparing for launch..."));
            parentMissionEvents.Add(new DeployEquipmentEvent(DeployEquipmentEvent.DeploymentState.RETRACTED));
            parentMissionEvents.Add(new SetEnginePowerEvent(1));
            parentMissionEvents.Add(new SASChangeActiveEvent(true));
            parentMissionEvents.Add(new ChangeSASModeEvent(KRPC.Client.Services.SpaceCenter.SASMode.StabilityAssist)); //beth says puppy
            parentMissionEvents.Add(new AnnouncementEvent("READY"));
            //COUNTDOWN
            parentMissionEvents.Add(new AnnouncementEvent("Launch in...."));
            parentMissionEvents.Add(new WaitForSecondsEvent(1, false));
            parentMissionEvents.Add(new WaitForSecondsEvent(5, true));
            //STAGE ONE
            parentMissionEvents.Add(new NextStageEvent()); //START BURN
            parentMissionEvents.Add(new WaitForSecondsEvent(2, true));
            parentMissionEvents.Add(new NextStageEvent());
            parentMissionEvents.Add(new EnsureAboveAltitudeEvent(5000));
            parentMissionEvents.Add(new InternalMissionEvent(internalGravityTurnMission, "Grav turn"));
            parentMissionEvents.Add(new SetEnginePowerEvent(0));
            //STAGE TWO
            parentMissionEvents.Add(new SetEnginePowerEvent(1));
            parentMissionEvents.Add(new SetEnginePowerEvent(0));
            parentMissionEvents.Add(new EnsureAboveAltitudeEvent(70500)); //wait for space and then...
            parentMissionEvents.Add(new EnableAutoPilotEvent(true));
            parentMissionEvents.Add(new TurnToHeadingEvent(0, 90));//point west, ready for orbital burn
            parentMissionEvents.Add(new WaitForApoapsisArrivalEvent(20)); //wait to 30secs prior to apoapsis
            parentMissionEvents.Add(new SetEnginePowerEvent(1)); //power to orbit
            parentMissionEvents.Add(new WaitUntilConditionEvent(new PeriapsisExceedsAltitudeCondition(70500)));
            parentMissionEvents.Add(new SetEnginePowerEvent(0)); //turn off engine
            //We are now in orbit
            //Let's cicularise it
            parentMissionEvents.Add(new EnableAutoPilotEvent(false));
            parentMissionEvents.Add(new SASChangeActiveEvent(true));
            parentMissionEvents.Add(new ChangeSASModeEvent(KRPC.Client.Services.SpaceCenter.SASMode.Retrograde));
            parentMissionEvents.Add(new WaitForPeriapsisArrivalEvent(5));
            parentMissionEvents.Add(new SetEnginePowerEvent(1));
            parentMissionEvents.Add(new WaitUntilConditionEvent(new PeriapsisBelowAltitudeCondition(80000)));
            parentMissionEvents.Add(new SetEnginePowerEvent(0));
            //Deployment
            parentMissionEvents.Add(new WaitForSecondsEvent(5, true));
            parentMissionEvents.Add(new NextStageEvent()); //lose fairing
            parentMissionEvents.Add(new DeployEquipmentEvent(DeployEquipmentEvent.DeploymentState.DEPLOYED)); //look cool
            parentMissionEvents.Add(new WaitForSecondsEvent(10, true));
            //Time to go home
            parentMissionEvents.Add(new NextStageEvent()); //leave satellite in space
            parentMissionEvents.Add(new WaitForSecondsEvent(5, true));//wait for drift a bit
            parentMissionEvents.Add(new NextStageEvent());
            parentMissionEvents.Add(new SetEnginePowerEvent(0.01f)); //move away slightly
            parentMissionEvents.Add(new WaitForSecondsEvent(5));
            parentMissionEvents.Add(new SetEnginePowerEvent(1)); //de orbit burn
            parentMissionEvents.Add(new WaitForSecondsEvent(10)); //10 secs should be long enough for de orbit, probs
            parentMissionEvents.Add(new SetEnginePowerEvent(0));
            parentMissionEvents.Add(new EnableAutoPilotEvent(false)); //no more turns so disable so we can get back SAS control
            parentMissionEvents.Add(new ChangeSASModeEvent(KRPC.Client.Services.SpaceCenter.SASMode.Retrograde)); //point backwards
            parentMissionEvents.Add(new WaitForSecondsEvent(5));//wait to point backwards
            parentMissionEvents.Add(new EnsureBelowAltitudeEvent(40000));
            parentMissionEvents.Add(new SetEnginePowerEvent(1)); //slow down a bit whilst we come in
            parentMissionEvents.Add(new WaitForFuelDepletedEvent());
            parentMissionEvents.Add(new EnsureBelowAltitudeEvent(3000));
            parentMissionEvents.Add(new ActivateParachutesEvent());
            missionOne = new Mission(parentMissionEvents, missionOutputTextBox, connectionTarget, aborts);
        }

        private async void EngageMissionOne()
        {
            try
            {
                await missionOne.StartMission();
            }
            catch(SocketException)
            {
                MessageBox.Show("Could not connect, ensure the server is open and the address is correct", "Could not connect", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            missionOutputTextBox.Text += "Mission complete";
        }

        private void GenerateMissionTwo()
        {
            List<MissionEvent> missionEvents = new List<MissionEvent>();
            //Countdown and engage SAS
            missionEvents.Add(new WaitForSecondsEvent(3, true));
            missionEvents.Add(new ChangeSASModeEvent(KRPC.Client.Services.SpaceCenter.SASMode.StabilityAssist));
            //Stage one
            missionEvents.Add(new NextStageEvent()); //engage engine
            missionEvents.Add(new NextStageEvent()); //release tower
            missionEvents.Add(new WaitForSecondsEvent(5, true)); //wait for 5 seconds
            missionEvents.Add(new ChangeSASModeEvent(KRPC.Client.Services.SpaceCenter.SASMode.Normal)); //go normal!
            missionEvents.Add(new WaitForSecondsEvent(5, true)); //wait for 5 seconds
            missionEvents.Add(new ChangeSASModeEvent(KRPC.Client.Services.SpaceCenter.SASMode.StabilityAssist));
            missionEvents.Add(new WaitForFuelDepletedEvent());
            //Decouple stage one
            missionEvents.Add(new NextStageEvent());
            //Stage two
            //Face command module retrograde for landing and wait for parachute height
            missionEvents.Add(new ChangeSASModeEvent(KRPC.Client.Services.SpaceCenter.SASMode.Retrograde));
            missionEvents.Add(new WaitForAltitudeAccurateEvent(900));
            //Parachute launch
            missionEvents.Add(new ActivateParachutesEvent());
            //Wait for landing
            missionEvents.Add(new WaitForAltitudeRangeEvent(55, 20, 10000));

            List<Abort> abortCriteria = new List<Abort>();
            List<Condition> checks = new List<Condition>();
            List<MissionEvent> abortInstructions = new List<MissionEvent>();

            checks.Add(new ExceededAltitudeCondition(1000));
            abortInstructions.Add(new SASChangeActiveEvent(false));
            abortInstructions.Add(new NextStageEvent());
            abortInstructions.Add(new NextStageEvent());
            abortInstructions.Add(new NextStageEvent());
            abortInstructions.Add(new NextStageEvent());
            abortInstructions.Add(new NextStageEvent());
            abortInstructions.Add(new ActivateParachutesEvent());
            abortCriteria.Add(new Abort(checks, abortInstructions, false));

            missionTwo = new Mission(missionEvents, missionOutputTextBox, System.Net.IPAddress.Parse("127.0.0.1"), abortCriteria);
        }

        private async void EngageMissionTwo()
        {
            try
            {
                await missionTwo.StartMission();
            }
            catch (SocketException)
            {
                MessageBox.Show("Could not connect, ensure the server is open and the address is correct", "Could not connect", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            missionOutputTextBox.Text += "Mission complete";
        }
        #endregion

        private void eventsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Type selectedType = allEvents[eventsComboBox.SelectedIndex];
            try
            {
                //We generically create an instance of the specified type using the default or an empty constructor.
                //The event property grid uses the public accessors on the objects to allow for editing
                //(This may mean that if your object isn't working you'll need to add an empty contructor + get/set for the properties you want to be editable)
                newEventPropertyGrid.SelectedObject = Activator.CreateInstance(selectedType); 
            }
            catch (MissingMethodException ex) //Catch not having an empty constructor
            {
                newEventPropertyGrid.SelectedObject = null;
                addEventButton.Enabled = false;
                MessageBox.Show("This event is not currently supported in the editor", "Unsupported event", MessageBoxButtons.OK, MessageBoxIcon.Information);
                System.Diagnostics.Debug.WriteLine("Unsupported editor type - " + selectedType.ToString() + ", error: " + ex.Message);
                return;
            }
            addEventButton.Enabled = true;
        }

        private void addEventButton_Click(object sender, EventArgs e)
        {
            missionEventsListBox.Items.Add(newEventPropertyGrid.SelectedObject);
            missionBuilder.AddEvent((MissionEvent)newEventPropertyGrid.SelectedObject);
            newEventPropertyGrid.SelectedObject = null;
            addEventButton.Enabled = false;
        }

        #region Drag and drop mission events reorder

        private void missionEventsListBox_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Clicks > 1)
            {
                missionEventsListBox_MouseDoubleClick(sender, e);
                return;
            }
            if (missionEventsListBox.SelectedItem == null)
            {
                return;
            }
            else
            {
                missionEventsListBox.DoDragDrop(new DraggedData(missionEventsListBox.SelectedItem), DragDropEffects.Move);
            }
        }

        private void missionEventsListBox_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void missionEventsListBox_DragDrop(object sender, DragEventArgs e)
        {
            System.Drawing.Point point = missionEventsListBox.PointToClient(new System.Drawing.Point(e.X, e.Y));
            int index = missionEventsListBox.IndexFromPoint(point);
            if (index < 0)
            {
                index = missionEventsListBox.Items.Count - 1;
            }
            object data = e.Data.GetData(typeof(DraggedData));
            DraggedData draggedData = (DraggedData)data;
            missionEventsListBox.Items.Remove(draggedData.data);
            missionEventsListBox.Items.Insert(index, draggedData.data);

            missionBuilder.MoveMissionEvent((MissionEvent)draggedData.data, index);
        }

        private void missionEventsListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            editEventButton_Click(sender, e);
        }

        #endregion

        private class DraggedData
        {
            public object data;
            public DraggedData(object data)
            {
                this.data = data;
            }
        }

        private void startMissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            missionOutputTextBox.Text ="Mission start...";
            missionBuilder.ServerAddress = IPAddress.Parse(Properties.Settings.Default.connectionIPAddress);
            missionBuilder.OutputTextbox = missionOutputTextBox;
            //Uncomment these to make MissionOne work
            //GenerateMissionOne();
            //missionBuilder.OverwriteMissionEvents(missionOne.MissionEvents);

            try
            {
                missionBuilder.ToMission().StartMission();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Could not build mission: " + ex.Message);
            }
            mainScreenTabControl.SelectedTab = mainScreenTabControl.TabPages[1];
        }

        private void editEventButton_Click(object sender, EventArgs e)
        {
            if(missionEventsListBox.SelectedItem != null)
            {
                newEventPropertyGrid.SelectedObject = missionBuilder.GetMissionEventAt(missionEventsListBox.SelectedIndex);
                addEventButton.Enabled = true;
            }
            else
            {
                MessageBox.Show("Please select the event to edit first");
            }
        }

        private void removeEventButton_Click(object sender, EventArgs e)
        {
            if (missionEventsListBox.SelectedItem != null)
            {
                missionEventsListBox.Items.RemoveAt(missionEventsListBox.SelectedIndex);
                missionBuilder.RemoveEventAt(missionEventsListBox.SelectedIndex);
            }
            else
            {
                MessageBox.Show("Please select the event to remove first");
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionsForm optionsForm = new OptionsForm();
            optionsForm.Show();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                if (missionBuilder.ServerAddress == null)
                {
                    missionBuilder.ServerAddress = IPAddress.Parse(Properties.Settings.Default.connectionIPAddress);
                }
                if (missionBuilder.OutputTextbox == null)
                {
                    missionBuilder.OutputTextbox = missionOutputTextBox;
                }

                string saveData = missionBuilder.ToMission().Serialize();
                //GenerateMissionOne();
                //string saveData = missionOne.Serialize();

                System.IO.File.WriteAllText(saveFileDialog.FileName, saveData);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string data = System.IO.File.ReadAllText(openFileDialog.FileName);
                data = data.Replace("encoding=\"utf-16\"", "");
                Mission mission = data.ParseXML<Mission>();
                List<MissionEvent> deserializedMisisonEvents = mission.MissionEvents;
                missionBuilder.OverwriteMissionEvents(deserializedMisisonEvents);

                missionEventsListBox.Items.Clear();
                foreach (MissionEvent missionEvent in deserializedMisisonEvents)
                {
                    missionEventsListBox.Items.Add(missionEvent);
                }
            }
        }
    }
}
