﻿using KRPC.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class NextStageEvent : MissionEvent
    {
        public NextStageEvent()
        {
            eventEnteredInfo = "Transitioning to next stage";
            eventExitedInfo = "Transition completed";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            mission.OutputToTextBoxCrossThread("Transition in progress");
            Vessel vessel = connection.SpaceCenter().ActiveVessel;
            vessel.Control.ActivateNextStage();
        }

        public override string ToString()
        {
            return "Next Stage Event";
        }
    }
}
