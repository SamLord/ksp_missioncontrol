﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class ActivateParachutesEvent : MissionEvent
    {
        public ActivateParachutesEvent() : base()
        {
            eventEnteredInfo = "Ready to deploy parachutes";
            eventExitedInfo = "Parachutes deployed";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            connection.SpaceCenter().ActiveVessel.Control.Parachutes = true;
        }

        public override string ToString()
        {
            return "Activate parachute event";
        }
    }
}
