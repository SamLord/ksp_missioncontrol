﻿using MissionControl.Missions.Aborts.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;

namespace MissionControl.Missions.Events
{
    public class ConditionalEvent : MissionEvent
    {
        private Condition condition;
        private Mission missionCriteriaTrue;
        private Mission missionCriteriaFalse;

        public Condition Condition
        {
            get
            {
                return condition;
            }
            set
            {
                condition = value;
            }
        }

        public Mission MissionCriteriaTrue
        {
            get
            {
                return missionCriteriaTrue;
            }
            set
            {
                missionCriteriaTrue = value;
            }
        }
        public Mission MissionCriteriaFalse
        {
            get
            {
                return missionCriteriaFalse;
            }
            set
            {
                missionCriteriaFalse = value;
            }
        }

        public ConditionalEvent() {}

        public ConditionalEvent(Condition condition, Mission missionCriteriaTrue, Mission missionCriteriaFalse)
        {
            this.condition = condition;
            this.missionCriteriaTrue = missionCriteriaTrue;
            this.missionCriteriaFalse = missionCriteriaFalse;
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            if (await condition.CriteriaMet(connection))
            {
                mission.OutputToTextBoxCrossThread("Conditional event (" + condition.ToString() + ") - TRUE");
                await missionCriteriaTrue.StartMission(connection);
            }
            else
            {
                mission.OutputToTextBoxCrossThread("Conditional event (" + condition.ToString() + ") - FALSE");
                await missionCriteriaFalse.StartMission(connection);
            }

        }

        public override string ToString()
        {
            return "Conditional event";
        }
    }
}
