﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class EnsureBelowAltitudeEvent : MissionEvent
    {
        private int altitude;

        public int Altitude
        {
            get
            {
                return altitude;
            }
            set
            {
                altitude = value;
            }
        }

        public EnsureBelowAltitudeEvent() { }

        public EnsureBelowAltitudeEvent(int altitude)
        {
            this.altitude = altitude;
            eventEnteredInfo = "Ensuring below " + altitude + "m";
            eventExitedInfo = "Below " + altitude + "m";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            while(connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude > altitude)
            {
                await Task.Delay(new TimeSpan(0, 0, 0, 0, 500));
            }
        }

        public override string ToString()
        {
            return "Ensuring below " + altitude + "m";
        }


    }
}
