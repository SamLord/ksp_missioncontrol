﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class SetEnginePowerEvent : MissionEvent
    {
        float newPower;

        public float Power
        {
            get
            {
                return newPower;
            }
            set
            {
                newPower = value;
            }
        }

        public SetEnginePowerEvent()
        {
            newPower = 0.5f;
        }

        public SetEnginePowerEvent(float powerLevel)
        {
            this.newPower = powerLevel;
            eventEnteredInfo = "Ready to change power level to " + powerLevel;
            eventExitedInfo = "Completed power change";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            connection.SpaceCenter().ActiveVessel.Control.Throttle = newPower;
        }

        public override string ToString()
        {
            return "Set throttle to " + newPower + " event";
        }
    }
}
