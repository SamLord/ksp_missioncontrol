﻿using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class SASChangeActiveEvent : MissionEvent
    {
        private bool enabled;

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

        public SASChangeActiveEvent()
        {
            this.enabled = true;
            eventEnteredInfo = "Ready to change SAS enabled";
            eventExitedInfo = "Set SAS";
        }

        public SASChangeActiveEvent(bool enabled)
        {
            this.enabled = enabled;
            eventEnteredInfo = "Ready to set SAS enabled = " + enabled.ToString();
            eventExitedInfo = "Set SAS enabled = " + enabled.ToString();
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            mission.OutputToTextBoxCrossThread("SAS enabled = " + enabled.ToString());
            connection.SpaceCenter().ActiveVessel.Control.SAS = enabled;
        }

        public override string ToString()
        {
            return "Set SAS enabled = " + enabled + " event";
        }
    }
}
