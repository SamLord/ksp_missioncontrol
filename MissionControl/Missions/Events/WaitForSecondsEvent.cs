﻿using KRPC.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissionControl.Missions.Events
{
    public class WaitForSecondsEvent : MissionEvent
    {
        private int waitTime;
        private bool outputCountdown;

        public int WaitTime
        {
            get
            {
                return waitTime;
            }set
            {
                waitTime = value;
            }
        }

        public bool OutputCountdown
        {
            get
            {
                return outputCountdown;
            }
            set
            {
                outputCountdown = value;
            }
        }

        public WaitForSecondsEvent()
        {
            waitTime = 10;
            outputCountdown = true;
        }

        public WaitForSecondsEvent(int waitTime, bool outputCountdown = false)
        {
            this.waitTime = waitTime;
            this.outputCountdown = outputCountdown;
            eventEnteredInfo = "Ready to wait for " + waitTime + " seconds";
            eventExitedInfo = "Transition completed";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            if (outputCountdown)
            {
                for(int i = waitTime; i >= 0; i--)
                {
                    mission.OutputToTextBoxCrossThread("T:" + i + "...");
                    await Task.Delay(new TimeSpan(0, 0, 1));
                }
            }
            else
            {
                await Task.Delay(new TimeSpan(0, 0, waitTime));
            }
        }

        public override string ToString()
        {
            return "Wait for " + waitTime + " seconds event";
        }
    }
}
