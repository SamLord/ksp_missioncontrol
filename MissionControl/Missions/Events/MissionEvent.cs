﻿using KRPC.Client;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using MissionControl.Editor;

namespace MissionControl.Missions.Events
{
    public abstract class MissionEvent
    {
        protected string eventEnteredInfo;
        protected string eventExitedInfo;

        #region Getters and setters
        public string EventEnteredInfo
        {
            get
            {
                return eventEnteredInfo;
            }
            set
            {
                eventEnteredInfo = value;
            }
        }

        public string EventExitedInfo
        {
            get
            {
                return eventExitedInfo;
            }
            set
            {
                eventExitedInfo = value;
            }
        }
        #endregion

        public virtual string OnEnter()
        {
            return eventEnteredInfo;
        }

        public virtual string OnExit()
        {
            return eventExitedInfo;
        }

        public virtual async Task Activate(Mission mission, Connection connection)
        {
            mission.OutputToTextBoxCrossThread("Empty event activated");
        }

        public override string ToString()
        {
            return "No event summary";
        }
    }
}
