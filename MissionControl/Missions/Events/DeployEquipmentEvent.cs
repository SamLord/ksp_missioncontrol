﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class DeployEquipmentEvent : MissionEvent
    {
        DeploymentState state;

        public DeploymentState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        public DeployEquipmentEvent()
        {
            state = DeploymentState.DEPLOYED;
        }

        public DeployEquipmentEvent(DeploymentState state)
        {
            this.state = state;
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            switch (state)
            {
                case DeploymentState.DEPLOYED:
                    connection.SpaceCenter().ActiveVessel.Control.SolarPanels = true;
                    connection.SpaceCenter().ActiveVessel.Control.Antennas = true;
                    mission.OutputToTextBoxCrossThread("Deploying Solar Panels and Antennas");
                    break;
                case DeploymentState.RETRACTED:
                    connection.SpaceCenter().ActiveVessel.Control.SolarPanels = false;
                    connection.SpaceCenter().ActiveVessel.Control.Antennas = false;
                    mission.OutputToTextBoxCrossThread("Retracting Solar Panels and Antennas");
                    break;
                default:
                    mission.OutputToTextBoxCrossThread("DeployEquipmentEvent failed - unknown state");
                    break;
            }
        }

        public override string ToString()
        {
            return "Set equipment to " + state.ToString();
        }

        public enum DeploymentState
        {
            DEPLOYED,
            RETRACTED
        }
    }
}
