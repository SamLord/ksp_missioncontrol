﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class WaitForAltitudeRangeEvent : MissionEvent
    {
        int tolerance;
        int targetAltitude;
        int checkFrequency;

        public int Tolerance
        {
            get { return tolerance; }
            set { tolerance = value; }
        }

        public int TargetAltitude
        {
            get { return targetAltitude; }
            set { targetAltitude = value; }
        }

        public int CheckFrequency
        {
            get { return checkFrequency; }
            set { checkFrequency = value; }
        }


        public WaitForAltitudeRangeEvent(int altitude, int tolerance = 250, int checkFrequencyMilliSeconds = 500)
        {
            this.targetAltitude = altitude;
            this.tolerance = tolerance;
            checkFrequency = checkFrequencyMilliSeconds;

            eventEnteredInfo = "Ready to wait for altitude reading of " + altitude + "m, with " + tolerance +
                "m tolerence";
            eventExitedInfo = "Reached altitude of " + altitude + " +/- " + tolerance;
        }

        protected WaitForAltitudeRangeEvent() { }

        public override async Task Activate(Mission mission, Connection connection)
        {
            while (Math.Abs(connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude - targetAltitude) > tolerance)
            {
                await Task.Delay(new TimeSpan(0, 0, 0, 0, checkFrequency));
            }
        }

        public override string ToString()
        {
            return "Altitude wait event for " + targetAltitude + "m, with " + tolerance + "m tolerence";
        }


    }
}
