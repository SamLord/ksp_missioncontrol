﻿using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;
using System;
using System.Threading.Tasks;

namespace MissionControl.Missions.Events
{
    public class WaitForSpeedEvent : MissionEvent
    {
        int tolerance;
        int targetSpeed;
        int checkFrequency;

        public int Tolerance
        {
            get { return tolerance; }
            set { tolerance = value; }
        }

        public int TargetSpeed
        {
            get { return targetSpeed; }
            set { targetSpeed = value; }
        }

        public int CheckFrequency
        {
            get { return checkFrequency; }
            set { checkFrequency = value; }
        }


        public WaitForSpeedEvent() { }

        public WaitForSpeedEvent(int targetSpeed, int tolerance = 250, int checkFrequencyMilliSeconds = 500)
        {
            this.targetSpeed = targetSpeed;
            this.tolerance = tolerance;
            checkFrequency = checkFrequencyMilliSeconds;

            eventEnteredInfo = "Ready to wait for speed reading of " + targetSpeed + "m/s, with " + tolerance +
                "m tolerence";
            eventExitedInfo = "Reached speed of " + targetSpeed + "m/s +/- " + tolerance;
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            while (Math.Abs(connection.SpaceCenter().ActiveVessel.Flight().TrueAirSpeed - targetSpeed) > tolerance)
            {
                await Task.Delay(new TimeSpan(0, 0, 0, 0, checkFrequency));
            }
        }

        public override string ToString()
        {
            return "Speed wait event for " + targetSpeed + "m/s, with " + tolerance + "m/s tolerence";
        }


    }
}
