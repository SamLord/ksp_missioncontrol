﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;

namespace MissionControl.Missions.Events
{
    public class InternalMissionEvent : MissionEvent
    {
        Mission innerMission;
        string name;

        public string MissionName
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public Mission InnerMission
        {
            get
            {
                return innerMission;
            }
            set
            {
                innerMission = value;
            }
        }

        public InternalMissionEvent() { }

        public InternalMissionEvent(Mission innerMission, string name)
        {
            this.innerMission = innerMission;
            this.name = name;
            eventEnteredInfo = "Starting internal Mission: " + name.ToUpper();
            eventExitedInfo = "Internal mission " + name.ToUpper() + " complete";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            if (innerMission != null)
            {
                mission.AssignTextBoxToMission(innerMission);
                await innerMission.StartMission(connection);
            }
        }

        public override string ToString()
        {
            return "Internal Mission - " + name;
        }
    }
}
