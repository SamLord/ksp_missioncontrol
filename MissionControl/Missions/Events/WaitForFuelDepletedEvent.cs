﻿using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissionControl.Missions.Events
{
    public class WaitForFuelDepletedEvent : MissionEvent
    {

        public WaitForFuelDepletedEvent()
        {
            eventEnteredInfo = "Ready to wait for fuel depletion";
            eventExitedInfo = "Wait for fuel depletion complete";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            mission.OutputToTextBoxCrossThread("Finding active engine");
            List<Engine> allEngines = (List<Engine>)connection.SpaceCenter().ActiveVessel.Parts.Engines;
            Engine activeEngine = null;
            foreach(Engine engine in allEngines)
            {
                if (engine.Active)
                {
                    activeEngine = engine;
                    break;
                }
            }

            if (activeEngine == null)
            {
                mission.OutputToTextBoxCrossThread("No active engine found. Aborting wait.");
                return;
            }
            mission.OutputToTextBoxCrossThread("Active engine found, waiting for fuel to deplete...");
            while (activeEngine.HasFuel)
            {
                await Task.Delay(new TimeSpan(0, 0, 1));
            }
        }

        public override string ToString()
        {
            return "Wait for fuel depleated event";
        }
    }
}
