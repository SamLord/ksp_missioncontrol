﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class ChangeSASModeEvent : MissionEvent
    {
        SASMode newMode;
        bool enableIfNotEnabled;

        public SASMode Mode
        {
            get
            {
                return newMode;
            }
            set
            {
                newMode = value;
            }
        }

        public bool AutoEnable
        {
            get
            {
                return enableIfNotEnabled;
            }
            set
            {
                enableIfNotEnabled = value;
            }
        }

        public ChangeSASModeEvent()
        {
            newMode = SASMode.StabilityAssist;
            enableIfNotEnabled = true;
        }

        public ChangeSASModeEvent(SASMode newMode, bool enableIfNotEnabled = true)
        {
            this.newMode = newMode;
            this.enableIfNotEnabled = enableIfNotEnabled;
            eventEnteredInfo = "Ready to change SAS mode to " + newMode.ToString();
            eventExitedInfo = "Completed SAS mode change";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            if (enableIfNotEnabled)
            {
                await mission.PerformEvent(new AnnouncementEvent("Auto-enabling SAS"), connection);
                await mission.PerformEvent(new SASChangeActiveEvent(true), connection);
            }
            connection.SpaceCenter().ActiveVessel.Control.SASMode = newMode;
            await mission.PerformEvent(new AnnouncementEvent("SAS Mode now " + connection.SpaceCenter().ActiveVessel.Control.SASMode), connection);
        }

        public override string ToString()
        {
            return "Change to " + newMode + " SAS mode event";
        }
    }
}