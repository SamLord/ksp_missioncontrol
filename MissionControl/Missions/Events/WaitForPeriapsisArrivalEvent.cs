﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class WaitForPeriapsisArrivalEvent : MissionEvent
    {
        int upToSecondsBefore;
        double timeToPeriapsis;

        public double TimeToPeriapsis
        {
            get { return timeToPeriapsis; }
            set { timeToPeriapsis = value; }
        }

        public int UpToSecondsBefore
        {
            get { return upToSecondsBefore; }
            set { upToSecondsBefore = value; }
        }

        public WaitForPeriapsisArrivalEvent()
        {

        }

        public WaitForPeriapsisArrivalEvent(int upToSecondsBefore)
        {
            this.upToSecondsBefore = upToSecondsBefore;
            eventEnteredInfo = "Waiting to arrive at periapsis";
            eventExitedInfo = "Arrived at periapsis";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            timeToPeriapsis = connection.SpaceCenter().ActiveVessel.Orbit.TimeToPeriapsis;
            while (timeToPeriapsis >= upToSecondsBefore)
            {
                timeToPeriapsis = connection.SpaceCenter().ActiveVessel.Orbit.TimeToPeriapsis;
                await Task.Delay(new TimeSpan(0, 0, 0, 0, 500));
            }
        }

        public override string ToString()
        {
            return "Wait for Periapsis arrival event";
        }
    }
}
