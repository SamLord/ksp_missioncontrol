﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class WaitForApoapsisArrivalEvent : MissionEvent
    {
        int upToSecondsBefore;
        double timeToApoapsis;
        
        public double TimeToApoapsis
        {
            get { return timeToApoapsis; }
            set { timeToApoapsis = value; }
        }
        public int UpToSecondsBefore
        {
            get { return upToSecondsBefore; }
            set { upToSecondsBefore = value; }
        }

        public WaitForApoapsisArrivalEvent()
        {
            this.upToSecondsBefore = 5;
            eventEnteredInfo = "Waiting to arrive at apoapsis";
            eventExitedInfo = "Arrived at apoapsis";
        }

        public WaitForApoapsisArrivalEvent(int upToSecondsBefore)
        {
            this.upToSecondsBefore = upToSecondsBefore;
            eventEnteredInfo = "Waiting to arrive at apoapsis";
            eventExitedInfo = "Arrived at apoapsis";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            timeToApoapsis = connection.SpaceCenter().ActiveVessel.Orbit.TimeToApoapsis;
            while (timeToApoapsis >= upToSecondsBefore)
            {
                timeToApoapsis = connection.SpaceCenter().ActiveVessel.Orbit.TimeToApoapsis;
                await Task.Delay(new TimeSpan(0, 0, 0, 0, 500));
            }
        }

        public override string ToString()
        {
            return "Wait for Apoapsis arrival event";
        }
    }
}
