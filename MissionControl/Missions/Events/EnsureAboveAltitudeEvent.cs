﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class EnsureAboveAltitudeEvent : MissionEvent
    {
        private int altitude;

        public int Altitude
        {
            get
            {
                return altitude;
            }
            set
            {
                altitude = value;
            }
        }

        public EnsureAboveAltitudeEvent() { }

        public EnsureAboveAltitudeEvent(int altitude)
        {
            this.altitude = altitude;
            eventEnteredInfo = "Ensuring above " + altitude + "m";
            eventExitedInfo = "Above " + altitude + "m";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            while (connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude < altitude)
            {
                await Task.Delay(new TimeSpan(0, 0, 0, 0, 500));
            }
        }

        public override string ToString()
        {
            return "Ensuring above " + altitude + "m";
        }


    }
}
