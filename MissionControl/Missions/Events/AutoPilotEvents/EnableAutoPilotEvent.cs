﻿using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events.AutoPilotEvents
{
    public class EnableAutoPilotEvent : MissionEvent
    {
        private bool enable;

        public bool Enable
        {
            get
            {
                return enable;
            }
            set
            {
                enable = value;
            }
        }

        public EnableAutoPilotEvent() { }

        public EnableAutoPilotEvent(bool enable)
        {
            this.enable = enable;
            eventEnteredInfo = "Ready to change auto pilot status";
            eventExitedInfo = "Autopilot enabled - " + enable.ToString().ToUpper();
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            if (enable)
            {
                connection.SpaceCenter().ActiveVessel.AutoPilot.Engage();
            }
            else
            {
                connection.SpaceCenter().ActiveVessel.AutoPilot.Disengage();
            }
        }

        public override string ToString()
        {
            return "Engaage autopilot event, set autopilot enabled to " + enable.ToString().ToUpper();
        }
    }
}
