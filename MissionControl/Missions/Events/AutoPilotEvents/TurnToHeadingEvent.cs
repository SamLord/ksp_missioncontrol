﻿using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events.AutoPilotEvents
{
    public class TurnToHeadingEvent : MissionEvent
    {
        private float heading;
        private float pitch;

        public float Heading
        {
            get
            {
                return heading;
            }
            set
            {
                heading = value;
            }
        }

        public float Pitch
        {
            get
            {
                return pitch;
            }
            set
            {
                pitch = value;
            }
        }

        public TurnToHeadingEvent() { }

        public TurnToHeadingEvent(float pitch, float heading)
        {
            this.heading = heading;
            this.pitch = pitch;
            eventEnteredInfo = "Starting heading change";
            eventExitedInfo = "Target heading changed";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            connection.SpaceCenter().ActiveVessel.AutoPilot.TargetPitchAndHeading(pitch, heading);
        }

        public override string ToString()
        {
            return "Turn to heading event - " + pitch + ":" + heading;
        }
    }
}
