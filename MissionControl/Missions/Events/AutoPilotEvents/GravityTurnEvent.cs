﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;
using MissionControl.Missions.Aborts.Conditions;

namespace MissionControl.Missions.Events.AutoPilotEvents
{
    public class GravityTurnEvent : MissionEvent
    {
        private int startAltitude;
        private int endAltitude;
        private bool autoStageOnFuelDepleted;

        public int StartAltitiude
        {
            get
            {
                return startAltitude;
            }
            set
            {
                startAltitude = value;
            }
        }
        public int EndAltitiude
        {
            get
            {
                return endAltitude;
            }
            set
            {
                endAltitude = value;
            }
        }

        public bool AutoStageOnFuelDepleted
        {
            get
            {
                return autoStageOnFuelDepleted;
            }
            set
            {
                autoStageOnFuelDepleted = value;
            }
        }

        public GravityTurnEvent() { }

        public GravityTurnEvent(int startAltitude, int endAltitude, bool autoStageOnFuelDepleted = false)
        {
            this.startAltitude = startAltitude;
            this.endAltitude = endAltitude;
            this.autoStageOnFuelDepleted = autoStageOnFuelDepleted;

            eventEnteredInfo = "Starting Grav Turn";
            eventExitedInfo = "Grav Turn Completed";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            ExceededAltitudeCondition atAltitudecondition = new ExceededAltitudeCondition(startAltitude);
            while (!await atAltitudecondition.CriteriaMet(connection))
            {
                await Task.Delay(500);
            }
            //at turn start altitude
            atAltitudecondition = new ExceededAltitudeCondition(endAltitude);
            double turnAngle = 0;
            while (!await atAltitudecondition.CriteriaMet(connection))
            {
                double altitude = connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude;
                if (startAltitude < altitude && altitude < endAltitude)
                {
                    double frac = (altitude - startAltitude) / (endAltitude - startAltitude);
                    double newTurnAngle = frac * 90;
                    if (Math.Abs(newTurnAngle - turnAngle) > 0.5)
                    {
                        turnAngle = newTurnAngle;
                        connection.SpaceCenter().ActiveVessel.AutoPilot.TargetPitchAndHeading(90 - (float)turnAngle, 90);
                    }
                }
                if (autoStageOnFuelDepleted)
                {
                    HasFuelCondition hasFuel = new HasFuelCondition();
                    if(!await hasFuel.CriteriaMet(connection))
                    {
                        await new NextStageEvent().Activate(mission, connection);
                        await new WaitForSecondsEvent(1).Activate(mission, connection);
                        await new NextStageEvent().Activate(mission, connection);
                    }
                }
            }
        }

        public override string ToString()
        {
            return "Gravity turn event between " + startAltitude + " and " + endAltitude;
        }
    }
}
