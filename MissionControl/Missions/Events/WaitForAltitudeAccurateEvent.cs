﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class WaitForAltitudeAccurateEvent : MissionEvent
    {
        int targetAltitude;

        public int TargetAltitude
        {
            get
            {
                return targetAltitude;
            }
            set
            {
                targetAltitude = value;
            }
        }

        public WaitForAltitudeAccurateEvent() { }

        public WaitForAltitudeAccurateEvent(int targetAltitude)
        {
            this.targetAltitude = targetAltitude;
            eventEnteredInfo = "Ready to wait for altitude of " + targetAltitude + "m (accurate)";
            eventExitedInfo = "Hit altitude of " + targetAltitude + "m (accurate)";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            //going up towards
            if (targetAltitude > connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude)
            {
                mission.OutputToTextBoxCrossThread("Waiting to increase to " + targetAltitude + "m");
                //while below target altitude
                while (targetAltitude - connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude > 0)
                {
                    await Task.Delay(new TimeSpan(0, 0, 0, 0, 200));
                }
            }
            else //going down towards
            {
                mission.OutputToTextBoxCrossThread("Waiting to decrease to " + targetAltitude + "m");
                //while above target altitude
                while (connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude - targetAltitude > 0)
                {
                    await Task.Delay(new TimeSpan(0, 0, 0, 0, 200));
                }
            }
        }

        public override string ToString()
        {
            return "Accurate wait for altitude of " + targetAltitude + "m";
        }
    }
}
