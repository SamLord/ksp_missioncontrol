﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using MissionControl.Missions.Aborts.Conditions;

namespace MissionControl.Missions.Events
{
    public class WaitUntilConditionEvent : MissionEvent
    {
        private Condition condition;

        public Condition WaitCondition
        {
            get
            {
                return condition;
            }
            set
            {
                condition = value;
            }
        }

        public WaitUntilConditionEvent()
        {
            eventEnteredInfo = "Waiting on condition";
            eventExitedInfo = "Condition met";
        }

        public WaitUntilConditionEvent(Condition condition)
        {
            this.condition = condition;
            eventEnteredInfo = "Waiting until condition is true - " + condition.ToString();
            eventExitedInfo = "Condition met - " + condition.ToString();
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            while (!await condition.CriteriaMet(connection))
            {
                await Task.Delay(new TimeSpan(0, 0, 0, 0, 500));
            }
        }

        public override string ToString()
        {
            return "Waiting until condition is true - " + condition.ToString();
        }
    }
}
