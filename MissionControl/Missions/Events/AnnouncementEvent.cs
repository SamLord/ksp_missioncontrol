﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;

namespace MissionControl.Missions.Events
{
    public class AnnouncementEvent : MissionEvent
    {
        private string announcement;

        public string Announcement
        {
            get
            {
                return announcement;
            }
            set
            {
                announcement = value;
            }
        }

        public AnnouncementEvent() { }

        public AnnouncementEvent(string announcement)
        {
            this.announcement = announcement;
        }

        public override string OnEnter()
        {
            return "";
        }

        public override string ToString()
        {
            return "Announcement Event";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            mission.OutputToTextBoxCrossThread(announcement);
        }
    }
}
