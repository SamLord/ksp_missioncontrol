﻿using System;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Events
{
    public class WaitForSpeedAccurateEvent : MissionEvent
    {
        int targetSpeed;

        public int TargetSpeed
        {
            get { return targetSpeed; }
            set { targetSpeed = value; }
        }

        public WaitForSpeedAccurateEvent() { }

        public WaitForSpeedAccurateEvent(int targetSpeed)
        {
            this.targetSpeed = targetSpeed;
            eventEnteredInfo = "Ready to wait for speed of " + targetSpeed + "m/s (accurate)";
            eventExitedInfo = "Hit speed of " + targetSpeed + "m/s (accurate)";
        }

        public override async Task Activate(Mission mission, Connection connection)
        {
            //going up towards
            if (targetSpeed > connection.SpaceCenter().ActiveVessel.Flight().TrueAirSpeed)
            {
                mission.OutputToTextBoxCrossThread("Waiting to increase to " + targetSpeed + "m/s");
                //while below target speed
                while (targetSpeed - connection.SpaceCenter().ActiveVessel.Flight().TrueAirSpeed > 0)
                {
                    await Task.Delay(new TimeSpan(0, 0, 0, 0, 200));
                }
            }
            else //going down towards
            {
                mission.OutputToTextBoxCrossThread("Waiting to decrease to " + targetSpeed + "m/s");
                //while above target speed
                while (connection.SpaceCenter().ActiveVessel.Flight().TrueAirSpeed - targetSpeed > 0)
                {
                    await Task.Delay(new TimeSpan(0, 0, 0, 0, 200));
                }
            }
        }

        public override string ToString()
        {
            return "Accurate wait for speed of " + targetSpeed + "m/s";
        }
    }
}
