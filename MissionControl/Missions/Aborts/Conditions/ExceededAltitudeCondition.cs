﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;
using MissionControl.Editor;

namespace MissionControl.Missions.Aborts.Conditions
{
    [System.ComponentModel.BrowsableAttribute(true)]
    [System.ComponentModel.EditorAttribute(typeof(ConditionEditor), typeof(System.Drawing.Design.UITypeEditor))]
    public class ExceededAltitudeCondition : Condition
    {
        private int maxAltitude;

        public int MaxAltitude
        {
            get
            {
                return maxAltitude;
            }
            set
            {
                maxAltitude = value;
            }
        }

        public ExceededAltitudeCondition() { }

        public ExceededAltitudeCondition(int maxAltitude) : base("Max altitude exceeded")
        {
            this.maxAltitude = maxAltitude;
        }

        public override async Task<bool> CriteriaMet(Connection connection)
        {
            return (connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude > maxAltitude);
        }
    }
}
