﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Aborts.Conditions
{
    public class PeriapsisExceedsAltitudeCondition : Condition
    {
        private int altitude;

        public int Altitude
        {
            get
            {
                return altitude;
            }
            set
            {
                altitude = value;
            }
        }

        public PeriapsisExceedsAltitudeCondition() { }

        public PeriapsisExceedsAltitudeCondition(int altitude) : base("Apoapsis below threshold")
        {
            this.altitude = altitude;
        }

        public override async Task<bool> CriteriaMet(Connection connection)
        {
            if(connection.SpaceCenter().ActiveVessel.Orbit.PeriapsisAltitude > altitude)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Periapsis exceeds " + altitude + " condition";
        }
    }
}
