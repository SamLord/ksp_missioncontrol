﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;
using MissionControl.Extensions;

namespace MissionControl.Missions.Aborts.Conditions
{
    public class SecondsPastCondition : Condition
    {
        private DateTime initTime;
        private float seconds = 0.0f;

        public float Seconds
        {
            get
            {
                return seconds;
            }
            set
            {
                seconds = value;
            }
        }

        public SecondsPastCondition()
            : base("Seconds complete")
        {
        }
        public SecondsPastCondition(float seconds)
            : base("Seconds complete")
        {
            this.seconds = seconds;
        }

        public override async Task<bool> CriteriaMet(Connection connection)
        {
            if (initTime == null)
            {
                initTime = DateTime.Now;
            }

            if ((initTime - DateTime.Now).TotalSeconds > seconds)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Has no fuel condition";
        }
    }
}
