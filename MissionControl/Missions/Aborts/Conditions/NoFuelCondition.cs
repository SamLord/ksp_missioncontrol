﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;
using MissionControl.Extensions;

namespace MissionControl.Missions.Aborts.Conditions
{
    public class NoFuelCondition : Condition
    {
        public NoFuelCondition() : base ("Has no fuel in current stage")
        {
        }

        public override async Task<bool> CriteriaMet(Connection connection)
        {
            Engine activeEngine = connection.SpaceCenter().ActiveVessel.GetActiveEngine();
            if (activeEngine != null)
            {
                return (!activeEngine.HasFuel);
            }
            else
            {
                return true;
            }
        }

        public override string ToString()
        {
            return "Has no fuel condition";
        }
    }
}
