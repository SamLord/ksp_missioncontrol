﻿using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissionControl.Missions.Aborts.Conditions
{
    public class PeriapsisBelowAltitudeCondition : Condition
    {
        private int altitude;

        public int Altitude
        {
            get
            {
                return altitude;
            }
            set
            {
                altitude = value;
            }
        }

        public PeriapsisBelowAltitudeCondition() { }

        public PeriapsisBelowAltitudeCondition(int altitude)
            : base("Apoapsis below threshold")
        {
            this.altitude = altitude;
        }

        public override async Task<bool> CriteriaMet(Connection connection)
        {
            if (connection.SpaceCenter().ActiveVessel.Orbit.PeriapsisAltitude < altitude)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Periapsis below " + altitude + " condition";
        }
    }
}
