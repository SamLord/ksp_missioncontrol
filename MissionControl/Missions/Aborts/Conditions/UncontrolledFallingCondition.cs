﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Aborts.Conditions
{
    public class UncontrolledFallingCondition : Condition
    {
        int activationAltitude;
        bool activated;
        int lastKnownAltitude = 0;

        public int ActivationAltitude
        {
            get
            {
                return activationAltitude;
            }
            set
            {
                activationAltitude = value;
            }
        }

        public UncontrolledFallingCondition() { }

        public UncontrolledFallingCondition(int activationAltitude) : base("Out of control fall")
        {
            this.activationAltitude = activationAltitude;
            activated = false;
        }

        public override async Task<bool> CriteriaMet(Connection connection)
        {
            double currentAltitude = connection.SpaceCenter().ActiveVessel.Flight().MeanAltitude;
            if (!activated)
            {   
                lastKnownAltitude = (int)currentAltitude;
                if (currentAltitude >= activationAltitude)
                {
                    activated = true;
                }
                else
                {
                    return false;
                }
            }

            if (activated)
            {
                if(currentAltitude < activationAltitude)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }
    }
}
