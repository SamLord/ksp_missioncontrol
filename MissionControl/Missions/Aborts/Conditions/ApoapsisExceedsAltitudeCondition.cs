﻿using System.Threading.Tasks;
using KRPC.Client;
using KRPC.Client.Services.SpaceCenter;

namespace MissionControl.Missions.Aborts.Conditions
{
    public class ApoapsisExceedsAltitudeCondition : Condition
    {
        private int altitude;


        public int Altitude
        {
            get
            {
                return altitude;
            }
            set
            {
                altitude = value;
            }
        }

        public ApoapsisExceedsAltitudeCondition() { }

        public ApoapsisExceedsAltitudeCondition(int altitude) : base("Apoapsis below threshold")
        {
            this.altitude = altitude;
        }

        public override async Task<bool> CriteriaMet(Connection connection)
        {
            if(connection.SpaceCenter().ActiveVessel.Orbit.ApoapsisAltitude > altitude)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Apoapsis exceeds " + altitude + " condition";
        }
    }
}
