﻿using KRPC.Client;
using MissionControl.Editor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissionControl.Missions.Aborts.Conditions
{
    [BrowsableAttribute(true)]
    [EditorAttribute(typeof(ConditionEditor), typeof(System.Drawing.Design.UITypeEditor))]
    //[TypeConverter(typeof(ExpandableObjectConverter))]
    public abstract class Condition
    {
        string reason;

        public string Reason
        {
            get
            {
                return reason;
            }
        }

        public Condition()
        {
            this.reason = "Condition met";
        }

        public Condition(string reason)
        {
            this.reason = reason;
        }

        public async virtual Task<bool> CriteriaMet(Connection connection)
        {
            return false;
        }
    }
}
