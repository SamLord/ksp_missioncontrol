﻿using KRPC.Client;
using MissionControl.Missions.Aborts.Conditions;
using MissionControl.Missions.Events;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MissionControl.Missions.Aborts
{
    public class Abort
    {
        List<Condition> abortCriteria;
        List<MissionEvent> abortInstructions;
        bool requireMatchAll;
        string lastAbortReason;

        public string LastAbortCheckReason
        {
            get
            {
                return lastAbortReason;
            }
        }
        
        public List<Condition> Criteria
        {
            get
            {
                return abortCriteria;
            }
            set
            {
                abortCriteria = value;
            }
        }

        public bool RequireMatchAll
        {
            get
            {
                return requireMatchAll;
            }
            set
            {
                requireMatchAll = value;
            }
        }

        public List<MissionEvent> AbortInstructions
        {
            get
            {
                return abortInstructions;
            }
            set
            {
                abortInstructions = value;
            }
        }

        public Abort() { }

        /// <summary>
        /// An collection of creteria and events
        /// </summary>
        /// <param name="abortCriteria">The criteria to check to see if we should abort</param>
        /// <param name="abortInstructions">The instructions to perform in case of an abort</param>
        /// <param name="requireMatchAll">Are all criteria required to cause an abort?</param>
        public Abort(List<Condition> abortCriteria, List<MissionEvent> abortInstructions, bool requireMatchAll = false)
        {
            this.abortCriteria = abortCriteria;
            this.abortInstructions = abortInstructions;
            this.requireMatchAll = requireMatchAll;
        }

        /// <summary>
        /// Based on the criteria should the 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ShouldAbort(Connection connection)
        {
            foreach(Condition check in abortCriteria)
            {
                bool criteriaMet = await check.CriteriaMet(connection);
                if (criteriaMet && requireMatchAll) //need all to be true, keep checking
                {
                    lastAbortReason = check.Reason;
                    continue;
                }
                else if(!criteriaMet && requireMatchAll) //need all true and criteria not met - don't abort!
                {
                    lastAbortReason = null;
                    return false;
                }
                else if(criteriaMet && !requireMatchAll) //need only one and this check's criteria says abort - abort!
                {
                    lastAbortReason = check.Reason;
                    return true;
                }
                else if (!criteriaMet && !requireMatchAll) //need only one but criteria not met, keep checking
                {
                    continue;
                }
            }

            if (requireMatchAll) //continued through all and all criteria met - abort time!
            {
                return true;
            }
            else //continued through all and no criteria met - no abort!
            {
                lastAbortReason = null;
                return false;
            }
        }

        public async Task PerformAbortProcedure(Connection connection, Mission mission)
        {
            mission.OutputToTextBoxCrossThread("ABORT - Reason:" + lastAbortReason);
            for (int i = 0; i < abortInstructions.Count; i++)
            {
                mission.OutputToTextBoxCrossThread(abortInstructions[i].OnEnter());
                await abortInstructions[i].Activate(mission, connection);
                mission.OutputToTextBoxCrossThread(abortInstructions[i].OnExit());
            }
        }
    }
}
