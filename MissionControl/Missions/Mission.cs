﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using KRPC.Client;
using MissionControl.Missions.Events;
using KRPC.Client.Services.SpaceCenter;
using MissionControl.Missions.Aborts;
using System.Linq;
using MissionControl.Missions.Aborts.Conditions;
using System.Xml.Serialization;

namespace MissionControl.Missions
{
    public class Mission
    {
        //Mission stuff
        private List<MissionEvent> missionEvents;
        private RichTextBox outputTextBox;

        private List<Aborts.Abort> abortCriterion;
        
        //Connection stuff
        private IPAddress serverAddress;
        private int rcpPort;
        private int streamPort;

        public List<MissionEvent> MissionEvents
        {
            get
            {
                if (missionEvents != null)
                {
                    return missionEvents.ToList();
                }
                else
                {
                    return new List<MissionEvent>();
                }
            }
            set
            {
                missionEvents = value;
            }
        }

        public List<Aborts.Abort> AbortCriterion
        {
            get
            {
                return abortCriterion;
            }
            set
            {
                abortCriterion = value;
            }
        }

        public Mission() { }

        public Mission(List<MissionEvent> missionEvents, RichTextBox outputTextBox, IPAddress serverAddress, List<Aborts.Abort> abortCriterion = null)
        {
            this.missionEvents = missionEvents;
            this.outputTextBox = outputTextBox;
            this.serverAddress = serverAddress;
            this.abortCriterion = abortCriterion;
            rcpPort = 50000;
            streamPort = 50001;
        }

        public Mission(List<MissionEvent> missionEvents, RichTextBox outputTextBox, IPAddress serverAddress, int rcpPort, int streamPort, List<Aborts.Abort> abortCriterion = null)
        {
            this.missionEvents = missionEvents;
            this.outputTextBox = outputTextBox;
            this.serverAddress = serverAddress;
            this.abortCriterion = abortCriterion;
            this.rcpPort = rcpPort;
            this.streamPort = streamPort;
        }

        public async Task StartMission(Connection connection = null)
        {
            if (connection == null)
            {
                try
                {
                    using (Connection serverConnection = new Connection("Mission Control Connection", serverAddress, rcpPort, streamPort))
                    {
                        for (int i = 0; i < missionEvents.Count; i++)
                        {
                            try
                            {
                                await MissionEvent(i, serverConnection);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Mission Event through exception: " + missionEvents[i].ToString(), ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    outputTextBox.Text += Environment.NewLine + "INTERNAL FAILURE: " + Environment.NewLine + ex.Message 
                        + Environment.NewLine + Environment.NewLine + ex.InnerException.Message;
                }
            }
            else
            {
                for (int i = 0; i < missionEvents.Count; i++)
                {
                    await MissionEvent(i, connection);
                }
            }
        }

        private async Task MissionEvent(int index, Connection serverConnection)
        {
            if (await AbortIfRequired(serverConnection))
            {
                return;
            }
            await PerformEvent(missionEvents[index], serverConnection);
        }

        private async Task<bool> AbortIfRequired(Connection serverConnection)
        {
            Abort trigger;
            trigger = await ShouldAbort(serverConnection);
            if (trigger != null)
            {
                await trigger.PerformAbortProcedure(serverConnection, this);
                return true;
            }
            return false;
        }

        public async Task PerformEvent(MissionEvent missionEvent, Connection serverConnection)
        {
            OutputToTextBoxCrossThread(missionEvent.OnEnter());
            await missionEvent.Activate(this, serverConnection);
            OutputToTextBoxCrossThread(missionEvent.OnExit());
        }

        private async Task<Abort> ShouldAbort(Connection connection)
        {
            if (abortCriterion != null)
            {
                foreach(Abort abort in abortCriterion)
                {
                    bool shouldAbort = await abort.ShouldAbort(connection);
                    if (shouldAbort)
                    {
                        return abort;
                    }
                }
            }
            return null;
        }

        public  void OutputToTextBoxCrossThread(string message)
        {
            if (outputTextBox != null)
            {
                outputTextBox.Invoke((MethodInvoker)delegate
                {
                    outputTextBox.AppendText(message + Environment.NewLine);
                });
            }
        }

        public Mission AssignTextBoxToMission(Mission mission)
        {
            mission.SetOutputTextBox(this.outputTextBox);
            return mission;
        }

        public void SetOutputTextBox(RichTextBox textBox)
        {
            outputTextBox = textBox;
        }

        public override string ToString()
        {
            string output = "";

            //events
            foreach(MissionEvent missionEvent in missionEvents)
            {
                output += missionEvent.ToString() + "->" + Environment.NewLine;
            }

            output += Environment.NewLine + "Abort criteria: " + Environment.NewLine + Environment.NewLine;

            //Abort stuff
            int count = 1;
            if (abortCriterion != null)
            {
                foreach (Abort abort in abortCriterion)
                {
                    output += "Abort checks (" + count + ")" + Environment.NewLine;
                    foreach (Condition check in abort.Criteria)
                    {
                        output += check.Reason + Environment.NewLine;
                    }
                    output += Environment.NewLine;
                    count++;
                }
            }
            return output;
        }
    }
}
