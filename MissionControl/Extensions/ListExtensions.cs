﻿using MissionControl.Missions.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissionControl.Extensions
{
    static class ListExtensions
    {

        public static void MoveItem<T>(this List<T> list, T item, int newIndex)
        {
            int oldIndex = -1;
            for(int i = 0; i< list.Count; i++)
            {
                if(item.Equals(list[i]))
                {
                    oldIndex = i;
                    break;
                }
            }

            if(oldIndex == -1)
            {
                throw new ArgumentException("The given item is not in the list");
            }

            list.Remove(item);

            if(oldIndex < newIndex) //removing the current item will effectively shunt the newIndex down one
            {
                //...so increase it
                newIndex++;
            }

            //If outside bounds of list, place at end
            if(newIndex >= list.Count || newIndex <= list.Count)
            {
                list.Add(item);
            }
            else //else insert it
            {
                list.Insert(newIndex, item);
            }
        }
    }
}
