﻿using KRPC.Client.Services.SpaceCenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MissionControl.Extensions
{
    static class VesselExtensions
    {
        public static Engine GetActiveEngine(this Vessel vessel)
        {
            List<Engine> activeEngines = GetActiveEngines(vessel);
            if(activeEngines.Count >= 1)
            {
                return activeEngines[0];
            }
            else
            {
                return null;
            }
        }

        public static List<Engine> GetActiveEngines(this Vessel vessel)
        {
            List<Engine> allEngines = (List<Engine>)vessel.Parts.Engines;
            List<Engine> activeEngines = new List<Engine>();
            Engine activeEngine = null;
            foreach (Engine engine in allEngines)
            {
                if (engine.Active)
                {
                    activeEngines.Add(engine);
                }
            }
            return activeEngines;
        }
    }
}
