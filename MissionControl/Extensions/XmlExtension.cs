﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MissionControl.Extensions
{
    public static class XmlExtension
    {
        public static string Serialize<T>(this T value)
        {
            if (value == null) return string.Empty;

            var xmlSerializer = new XmlSerializer(typeof(T), value.GetType().IterativeGetDerivedTypesFromProperties());
            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings { Indent = true }))
                {
                    xmlSerializer.Serialize(xmlWriter, value);
                    return stringWriter.ToString();

                }
            }
        }

        public static Stream ToStream(this string @this)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(@this);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static Type ToType(this string value, Type[] availableTypes)
        {
            Type type = Type.GetType(value);

            if(type == null)
            {
                foreach(Type availableType in availableTypes)
                {
                    if(availableType.Name == value)
                    {
                        type = availableType;
                        break;
                    }
                }
            }
            return type;
        }        

        public static T ParseXML<T>(this string value) where T : class
        {
            ////XmlReader reader = XmlReader.Create(value.Trim().ToStream(), new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
            //StringReader stringReader = new StringReader(value);
            Type[] derivedTypes = typeof(T).IterativeGetDerivedTypesFromProperties();            
            ////XmlSerializer xmlSerializer = new XmlSerializer(typeof(T), derivedTypes);
            ////XmlSerializer xmlSerializer = new XmlSerializer(typeof(T),new XmlAttributeOverrides(), derivedTypes, new XmlRootAttribute("Mission"),"");
            ////XmlSerializer xmlSerializer = new XmlSerializer(typeof(T), new XmlRootAttribute("Mission"));
            //XmlSerializer xmlSerializer = new XmlSerializer(typeof(T), derivedTypes);
            //return xmlSerializer.Deserialize(stringReader) as T;

            XmlDocument xd = new XmlDocument();
            xd.Load(value.Trim().ToStream());
            
            Type rootNodeType = typeof(T);
            XmlNode rootNode = xd.SelectNodes(rootNodeType.Name)[0];
            
            object contructedObject = ConstructObject(rootNodeType, rootNode, derivedTypes);
            return (T)contructedObject;
        }

        private static object ConstructObject(Type type, XmlNode equivalentNode, Type[] availableTypes)
        {
            object parent;
            if (type == typeof(string))
            {
                parent = "";
            }
            else
            {
                parent = Activator.CreateInstance(type);
            }

            PropertyInfo[] properties = type.GetProperties();

            foreach(PropertyInfo property in properties)
            {
                foreach(XmlNode childNode in equivalentNode.ChildNodes)
                {
                    //Found matching property in XML
                    if(property.Name == childNode.Name)
                    {
                        Type propertyType = property.PropertyType;
                        object propertyValue = null;

                        if(propertyType == typeof(string)) //strings don't play nice, so we deal with them explicitly
                        {
                            propertyValue = childNode.InnerText;
                        }
                        else if (propertyType.IsPrimitive)
                        {
                            propertyValue = Convert.ChangeType(childNode.InnerText, propertyType);
                        }
                        //List, probs
                        else if (propertyType.IsGenericType)
                        {
                            Type listType = typeof(List<>);
                            Type constructedListType = listType.MakeGenericType(propertyType.GetGenericArguments()[0]);

                            IList constructedList = (IList) Activator.CreateInstance(constructedListType);
                            Type[] listGenericArguments = propertyType.GetGenericArguments();
                            propertyValue = FillListFromXML(constructedList, childNode, listGenericArguments[0], availableTypes);
                        }
                        //some other object, lets go recursive!
                        else
                        {
                            //Allow for abstract/inherited classes by checking for xsi:type attributes
                            Type derivedType = propertyType;
                            foreach (XmlNode attribute in childNode.Attributes)
                            {
                                if (attribute.Name == "xsi:type")
                                {
                                    derivedType = attribute.Value.ToType(availableTypes);
                                    break;
                                }
                            }
                            propertyValue = ConstructObject(derivedType, childNode, availableTypes);
                        }


                        property.SetValue(parent, propertyValue);
                    }
                }
            }

            return parent;
        }

        private static IList FillListFromXML(IList list, XmlNode listNode, Type heldByListType, Type[] availableTypes)
        {
            Type baseListType = heldByListType;
            foreach(XmlNode childNode in listNode.ChildNodes)
            {
                if (baseListType.IsAbstract)
                {
                    //need to extract derived type from each node
                    foreach(XmlNode attribute in childNode.Attributes)
                    {
                        if(attribute.Name == "xsi:type")
                        {
                            heldByListType = attribute.Value.ToType(availableTypes);
                            break;
                        }
                    }
                }
                object builtObject = ConstructObject(heldByListType, childNode, availableTypes);
                
                //IList kicks up a fuss when adding derived types, so we do some reflection to cast them to the types we want
                //MethodInfo castMethod = typeof(XmlExtension).GetMethod("Cast").MakeGenericMethod(baseListType);
                //object castedObject = castMethod.Invoke(null, new object[] { builtObject });
                
                list.Add(builtObject);
            }
            return list;
        }

        public static T Cast<T>(object o)
        {
            return (T)o;
        }

        private static Type[] IterativeGetDerivedTypesFromProperties(this Type type)
        {
            List<Type> foundTypes = new List<Type>();
            List<Type> checkedTypes = new List<Type>();

            foundTypes.Add(type);

            while (checkedTypes.Count != foundTypes.Count)
            {
                List<Type> newTypes = new List<Type>();
                foreach (Type foundType in foundTypes)
                {
                    if (!checkedTypes.Contains(foundType))
                    {
                        checkedTypes.Add(foundType);
                        newTypes.AddRange(foundType.GetDerivedTypesFromProperties());
                    }
                }
                foundTypes.AddRange(newTypes);
                foundTypes = foundTypes.Distinct().ToList();
            }

            return foundTypes.ToArray();
        }

        private static Type[] GetDerivedTypesFromProperties(this Type type)
        {
            List<Type> types = new List<Type>();
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Public);
            properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Type propertyType = property.PropertyType;

                //Check if list, if so get type held by list, not list itself
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(List<>))
                {
                    propertyType = propertyType.GetGenericArguments()[0];
                }
                types.Add(propertyType);
                types.AddRange(propertyType.GetDerivedTypes());
            }

            return types.Distinct().ToArray();
        }

        private static Type[] GetDerivedTypes(this Type initialType)
        {
            Type[] derivedTypes = (from domainAssembly in AppDomain.CurrentDomain.GetAssemblies()
                                   from assemblyType in domainAssembly.GetTypes()
                                   where initialType.IsAssignableFrom(assemblyType)
                                   select assemblyType).ToArray();
            return derivedTypes;
        }
    }
}
