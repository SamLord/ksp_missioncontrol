﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using MissionControl.Extensions;

namespace MissionControl.Editor
{
    class ConditionEditor : UITypeEditor
    {
        //http://stackoverflow.com/questions/4305033/property-grid-create-new-instance-on-a-property
        //https://msdn.microsoft.com/en-us/library/system.drawing.design.uitypeeditor.aspx

        private IWindowsFormsEditorService _editorService;
        private List<Type> allValidTypes;
        private ListBox listBox;
        private Type previousType;

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {

            _editorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

            if (listBox == null)
            {
                listBox = new ListBox();
                listBox.SelectionMode = SelectionMode.One;
                listBox.SelectedValueChanged += OnListBoxSelectedValueChanged;

                if (allValidTypes == null) // this reflection stuff can be quite expensive so lets not do it more often than necessary
                {
                    allValidTypes = Assembly.GetExecutingAssembly().GetNonAbstractTypesInNamespace("MissionControl.Missions.Aborts.Conditions");
                }

                foreach (Type type in allValidTypes)
                {
                    string[] splitNameSpace = type.ToString().Split('.');
                    string name = splitNameSpace[splitNameSpace.Length - 1];
                    listBox.Items.Add(name);
                }
            }

            _editorService.DropDownControl(listBox);

            if (listBox.SelectedItem == null)
            {
                return base.EditValue(context, provider, value); // no selection, no change
            }
            //if the selection is of the same type as last time then just edit the current value
            else if (value != null && previousType == allValidTypes[listBox.SelectedIndex])
            {
                return DisplayEditor(value);
            }

            // instantiate an object
            try
            {
                value = Activator.CreateInstance(allValidTypes[listBox.SelectedIndex]);
                previousType = allValidTypes[listBox.SelectedIndex];
            }
            catch (MissingMethodException ex)
            {
                MessageBox.Show("The selected condition has no paramterless constructor and so cannot be instantiated in this way", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                value = null;
                return base.EditValue(context, provider, value);
            }

            return DisplayEditor(value);

        }

        private object DisplayEditor(object value)
        {
            
            using (PropertyGridForm editor = new PropertyGridForm(value))
            {
                _editorService.ShowDialog(editor);
                if (editor.DialogResult == DialogResult.OK)
                {
                    value = editor.EditableObject;
                }
            }
            return value;
        }

        private void OnListBoxSelectedValueChanged(object sender, EventArgs e)
        {

            _editorService.CloseDropDown();
        }
    }
}
