﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MissionControl.Editor
{
    public partial class PropertyGridForm : Form
    {
        public object EditableObject;

        public PropertyGridForm(object displayObject)
        {
            Initalize(displayObject);
        }

        public PropertyGridForm(Type type)
        {
            Initalize(Activator.CreateInstance(type));
        }

        private void Initalize(object displayObject)
        {
            InitializeComponent();

            this.EditableObject = displayObject;
            propertyGrid.SelectedObject = EditableObject;
            propertyGrid.PropertyValueChanged += propertyGrid_PropertyValueChanged;

            this.Text = displayObject.ToString();
        }

        void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            this.Text = EditableObject.ToString();
        }
    }
}
