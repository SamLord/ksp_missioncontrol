﻿using MissionControl.Missions;
using MissionControl.Missions.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using MissionControl.Missions.Aborts;
using MissionControl.Extensions;

namespace MissionControl.Editor
{
    class MissionBuilder
    {
        IPAddress serverAddress;
        RichTextBox outputTextbox;
        List<MissionEvent> missionEvents;
        List<Abort> abortCriteria;

        #region Getters and Setters
        public int EventCount
        {
            get
            {
                return missionEvents.Count;
            }
        }

        public RichTextBox OutputTextbox
        {
            get
            {
                return outputTextbox;
            }
            set
            {
                outputTextbox = value;
            }
        }

        public IPAddress ServerAddress
        {
            get
            {
                return serverAddress;
            }
            set
            {
                serverAddress = value;
            }
        }
        #endregion

        #region Constructors
        public MissionBuilder()
        {
            missionEvents = new List<MissionEvent>();
            List<Abort> abortCriteria = new List<Abort>();
        }

        public MissionBuilder(RichTextBox outputTextbox)
        {
            missionEvents = new List<MissionEvent>();
            List<Abort> abortCriteria = new List<Abort>();
            this.outputTextbox = outputTextbox;
        }

        public MissionBuilder(IPAddress serverAddress, RichTextBox outputTextbox)
        {
            missionEvents = new List<MissionEvent>();
            List<Abort> abortCriteria = new List<Abort>();
            this.serverAddress = serverAddress;
            this.outputTextbox = outputTextbox;
        }

        public MissionBuilder(List<MissionEvent> missionEvents, RichTextBox outputTextbox, IPAddress serverAddress, List<Abort> abortCriteria)
        {
            this.missionEvents = missionEvents;
            this.outputTextbox = outputTextbox;
            this.serverAddress = serverAddress;
            this.abortCriteria = abortCriteria;
        }
        #endregion

        #region MissionEvents
        public void AddEvent(MissionEvent missionEvent)
        {
            missionEvents.Add(missionEvent);
        }

        public void AddEventAt(int index, MissionEvent missionEvent)
        {
            missionEvents.Insert(index, missionEvent);
        }

        public void RemoveEventAt(int index)
        {
            missionEvents.Remove(missionEvents[index]);
        }

        public void OverwriteMissionEvents(List<MissionEvent> missionEvents)
        {
            this.missionEvents = missionEvents;
        }

        public void MoveMissionEvent(MissionEvent missionEvent, int index)
        {
            missionEvents.MoveItem(missionEvent, index);
        }

        public MissionEvent GetMissionEventAt(int index)
        {
            return missionEvents[index];
        }

        public MissionEvent PopMissionEventAt(int index)
        {
            MissionEvent missionEvent = missionEvents[index];
            missionEvents.RemoveAt(index);
            return missionEvent;
        }
        #endregion

        #region Aborts
        public void AddAbort(Abort abort)
        {
            abortCriteria.Add(abort);
        }

        public void RemoveAbort(Abort abort)
        {
            abortCriteria.Remove(abort);
        }

        public void OverwriteAbortCriteria(List<Abort> abortCriteria)
        {
            this.abortCriteria = abortCriteria;
        }

        public void MoveAbort(Abort abort, int index)
        {
            abortCriteria.MoveItem(abort, index);
        }
        #endregion
        

        public Mission ToMission()
        {
            if(serverAddress == null || outputTextbox == null ||missionEvents == null|| missionEvents.Count == 0)
            {
                throw new ArgumentException("Must have server address, output text box and at least one mission event");
            }

            return new Mission(missionEvents, outputTextbox, serverAddress, abortCriteria);
        }
    }
}
