﻿namespace MissionControl
{
    partial class MissionControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.outputBoxLabel = new System.Windows.Forms.Label();
            this.missionOutputTextBox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.missionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startMissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainScreenTabControl = new System.Windows.Forms.TabControl();
            this.missionPlanningTab = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.removeEventButton = new System.Windows.Forms.Button();
            this.editEventButton = new System.Windows.Forms.Button();
            this.missionEventsListBox = new System.Windows.Forms.ListBox();
            this.addEventButton = new System.Windows.Forms.Button();
            this.eventsBoxNameLabel = new System.Windows.Forms.Label();
            this.eventsComboBox = new System.Windows.Forms.ComboBox();
            this.newEventPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.missionOutputTab = new System.Windows.Forms.TabPage();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.mainScreenTabControl.SuspendLayout();
            this.missionPlanningTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.missionOutputTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // outputBoxLabel
            // 
            this.outputBoxLabel.AutoSize = true;
            this.outputBoxLabel.Location = new System.Drawing.Point(11, 2);
            this.outputBoxLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.outputBoxLabel.Name = "outputBoxLabel";
            this.outputBoxLabel.Size = new System.Drawing.Size(39, 13);
            this.outputBoxLabel.TabIndex = 0;
            this.outputBoxLabel.Text = "Output";
            // 
            // missionOutputTextBox
            // 
            this.missionOutputTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.missionOutputTextBox.Location = new System.Drawing.Point(14, 19);
            this.missionOutputTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.missionOutputTextBox.Name = "missionOutputTextBox";
            this.missionOutputTextBox.ReadOnly = true;
            this.missionOutputTextBox.Size = new System.Drawing.Size(594, 343);
            this.missionOutputTextBox.TabIndex = 1;
            this.missionOutputTextBox.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.missionsToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(635, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // missionsToolStripMenuItem
            // 
            this.missionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startMissionToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.missionsToolStripMenuItem.Name = "missionsToolStripMenuItem";
            this.missionsToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.missionsToolStripMenuItem.Text = "Missions";
            // 
            // startMissionToolStripMenuItem
            // 
            this.startMissionToolStripMenuItem.Name = "startMissionToolStripMenuItem";
            this.startMissionToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.startMissionToolStripMenuItem.Text = "Start Mission";
            this.startMissionToolStripMenuItem.Click += new System.EventHandler(this.startMissionToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.saveToolStripMenuItem.Text = "Save...";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.loadToolStripMenuItem.Text = "Load...";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.optionsToolStripMenuItem.Text = "Options....";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // mainScreenTabControl
            // 
            this.mainScreenTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainScreenTabControl.Controls.Add(this.missionPlanningTab);
            this.mainScreenTabControl.Controls.Add(this.missionOutputTab);
            this.mainScreenTabControl.Location = new System.Drawing.Point(9, 25);
            this.mainScreenTabControl.Margin = new System.Windows.Forms.Padding(2);
            this.mainScreenTabControl.Name = "mainScreenTabControl";
            this.mainScreenTabControl.SelectedIndex = 0;
            this.mainScreenTabControl.Size = new System.Drawing.Size(617, 389);
            this.mainScreenTabControl.TabIndex = 3;
            // 
            // missionPlanningTab
            // 
            this.missionPlanningTab.Controls.Add(this.splitContainer1);
            this.missionPlanningTab.Location = new System.Drawing.Point(4, 22);
            this.missionPlanningTab.Margin = new System.Windows.Forms.Padding(2);
            this.missionPlanningTab.Name = "missionPlanningTab";
            this.missionPlanningTab.Padding = new System.Windows.Forms.Padding(2);
            this.missionPlanningTab.Size = new System.Drawing.Size(609, 363);
            this.missionPlanningTab.TabIndex = 1;
            this.missionPlanningTab.Text = "Mission Planning";
            this.missionPlanningTab.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(2, 2);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.removeEventButton);
            this.splitContainer1.Panel1.Controls.Add(this.editEventButton);
            this.splitContainer1.Panel1.Controls.Add(this.missionEventsListBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.addEventButton);
            this.splitContainer1.Panel2.Controls.Add(this.eventsBoxNameLabel);
            this.splitContainer1.Panel2.Controls.Add(this.eventsComboBox);
            this.splitContainer1.Panel2.Controls.Add(this.newEventPropertyGrid);
            this.splitContainer1.Size = new System.Drawing.Size(605, 359);
            this.splitContainer1.SplitterDistance = 201;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 0;
            // 
            // removeEventButton
            // 
            this.removeEventButton.Location = new System.Drawing.Point(127, 297);
            this.removeEventButton.Margin = new System.Windows.Forms.Padding(2);
            this.removeEventButton.Name = "removeEventButton";
            this.removeEventButton.Size = new System.Drawing.Size(56, 19);
            this.removeEventButton.TabIndex = 2;
            this.removeEventButton.Text = "Remove";
            this.removeEventButton.UseVisualStyleBackColor = true;
            this.removeEventButton.Click += new System.EventHandler(this.removeEventButton_Click);
            // 
            // editEventButton
            // 
            this.editEventButton.Location = new System.Drawing.Point(16, 297);
            this.editEventButton.Margin = new System.Windows.Forms.Padding(2);
            this.editEventButton.Name = "editEventButton";
            this.editEventButton.Size = new System.Drawing.Size(56, 19);
            this.editEventButton.TabIndex = 1;
            this.editEventButton.Text = "Edit";
            this.editEventButton.UseVisualStyleBackColor = true;
            this.editEventButton.Click += new System.EventHandler(this.editEventButton_Click);
            // 
            // missionEventsListBox
            // 
            this.missionEventsListBox.AllowDrop = true;
            this.missionEventsListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.missionEventsListBox.FormattingEnabled = true;
            this.missionEventsListBox.Location = new System.Drawing.Point(3, 3);
            this.missionEventsListBox.Margin = new System.Windows.Forms.Padding(2);
            this.missionEventsListBox.Name = "missionEventsListBox";
            this.missionEventsListBox.Size = new System.Drawing.Size(197, 277);
            this.missionEventsListBox.TabIndex = 0;
            this.missionEventsListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.missionEventsListBox_DragDrop);
            this.missionEventsListBox.DragOver += new System.Windows.Forms.DragEventHandler(this.missionEventsListBox_DragOver);
            this.missionEventsListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.missionEventsListBox_MouseDoubleClick);
            this.missionEventsListBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.missionEventsListBox_MouseDown);
            // 
            // addEventButton
            // 
            this.addEventButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addEventButton.Location = new System.Drawing.Point(236, 24);
            this.addEventButton.Margin = new System.Windows.Forms.Padding(2);
            this.addEventButton.Name = "addEventButton";
            this.addEventButton.Size = new System.Drawing.Size(76, 19);
            this.addEventButton.TabIndex = 4;
            this.addEventButton.Text = "Add Event";
            this.addEventButton.UseVisualStyleBackColor = true;
            this.addEventButton.Click += new System.EventHandler(this.addEventButton_Click);
            // 
            // eventsBoxNameLabel
            // 
            this.eventsBoxNameLabel.AutoSize = true;
            this.eventsBoxNameLabel.Location = new System.Drawing.Point(2, 7);
            this.eventsBoxNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.eventsBoxNameLabel.Name = "eventsBoxNameLabel";
            this.eventsBoxNameLabel.Size = new System.Drawing.Size(99, 13);
            this.eventsBoxNameLabel.TabIndex = 2;
            this.eventsBoxNameLabel.Text = "Choose New Event";
            // 
            // eventsComboBox
            // 
            this.eventsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.eventsComboBox.FormattingEnabled = true;
            this.eventsComboBox.Location = new System.Drawing.Point(4, 24);
            this.eventsComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.eventsComboBox.Name = "eventsComboBox";
            this.eventsComboBox.Size = new System.Drawing.Size(154, 21);
            this.eventsComboBox.TabIndex = 1;
            this.eventsComboBox.SelectedIndexChanged += new System.EventHandler(this.eventsComboBox_SelectedIndexChanged);
            // 
            // newEventPropertyGrid
            // 
            this.newEventPropertyGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newEventPropertyGrid.Location = new System.Drawing.Point(4, 48);
            this.newEventPropertyGrid.Margin = new System.Windows.Forms.Padding(2);
            this.newEventPropertyGrid.Name = "newEventPropertyGrid";
            this.newEventPropertyGrid.Size = new System.Drawing.Size(185, 300);
            this.newEventPropertyGrid.TabIndex = 0;
            // 
            // missionOutputTab
            // 
            this.missionOutputTab.Controls.Add(this.outputBoxLabel);
            this.missionOutputTab.Controls.Add(this.missionOutputTextBox);
            this.missionOutputTab.Location = new System.Drawing.Point(4, 22);
            this.missionOutputTab.Margin = new System.Windows.Forms.Padding(2);
            this.missionOutputTab.Name = "missionOutputTab";
            this.missionOutputTab.Padding = new System.Windows.Forms.Padding(2);
            this.missionOutputTab.Size = new System.Drawing.Size(609, 363);
            this.missionOutputTab.TabIndex = 0;
            this.missionOutputTab.Text = "Mission Output";
            this.missionOutputTab.UseVisualStyleBackColor = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // MissionControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 424);
            this.Controls.Add(this.mainScreenTabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MissionControlForm";
            this.Text = "Mission Control";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.mainScreenTabControl.ResumeLayout(false);
            this.missionPlanningTab.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.missionOutputTab.ResumeLayout(false);
            this.missionOutputTab.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label outputBoxLabel;
        private System.Windows.Forms.RichTextBox missionOutputTextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem missionsToolStripMenuItem;
        private System.Windows.Forms.TabControl mainScreenTabControl;
        private System.Windows.Forms.TabPage missionOutputTab;
        private System.Windows.Forms.TabPage missionPlanningTab;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox missionEventsListBox;
        private System.Windows.Forms.PropertyGrid newEventPropertyGrid;
        private System.Windows.Forms.ComboBox eventsComboBox;
        private System.Windows.Forms.Label eventsBoxNameLabel;
        private System.Windows.Forms.Button addEventButton;
        private System.Windows.Forms.ToolStripMenuItem startMissionToolStripMenuItem;
        private System.Windows.Forms.Button removeEventButton;
        private System.Windows.Forms.Button editEventButton;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}

