﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MissionControl
{
    public partial class OptionsForm : Form
    {
        public OptionsForm()
        {
            InitializeComponent();

            //Load current saved valued;
            ipAddressTextBox.Text = Properties.Settings.Default.connectionIPAddress;
            rcpPortTextBox.Text = Properties.Settings.Default.connectionRCPPort.ToString();
            streamPortTextBox.Text = Properties.Settings.Default.connectionStreamPort.ToString();
        }

        private void saveAndCloseButton_Click(object sender, EventArgs e)
        {
            IPAddress ipAddress;

            if (!IPAddress.TryParse(ipAddressTextBox.Text, out ipAddress))
            {
                ShowWarning("IP Address invalid", "IP Address Error");
                return;
            }
            else
            {
                Properties.Settings.Default.connectionIPAddress = ipAddress.ToString();
            }

            int rcpPort;
            if (!ValidatePort(rcpPortTextBox.Text, out rcpPort))
            {
                ShowWarning("RCP Port must be a number", "RCP Port error");
                return;
            }
            else
            {
                Properties.Settings.Default.connectionRCPPort = rcpPort;
            }

            int streamPort;
            if (!ValidatePort(streamPortTextBox.Text, out streamPort))
            {
                ShowWarning("Stream Port must be a number", "Stream Port error");
                return;
            }
            else
            {
                Properties.Settings.Default.connectionRCPPort = rcpPort;
            }

            this.Close();
        }

        private bool ValidatePort(string portText, out int port)
        {
            if (string.IsNullOrEmpty(portText) || string.IsNullOrWhiteSpace(portText)
                || !int.TryParse(rcpPortTextBox.Text, out port))
            {
                port = -1;
                return false;
            }
            else
            {
                return true;
            }
        }

        private void ShowWarning(string body, string title)
        {
            MessageBox.Show(body, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void justCloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
