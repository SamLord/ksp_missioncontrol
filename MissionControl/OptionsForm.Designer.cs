﻿namespace MissionControl
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveAndCloseButton = new System.Windows.Forms.Button();
            this.connectionOptionsTab = new System.Windows.Forms.TabPage();
            this.streamPortTextBox = new System.Windows.Forms.TextBox();
            this.rcpPortTextBox = new System.Windows.Forms.TextBox();
            this.ipAddressTextBox = new System.Windows.Forms.TextBox();
            this.streamPortLabel = new System.Windows.Forms.Label();
            this.rcpPortLabel = new System.Windows.Forms.Label();
            this.ipAddressLabel = new System.Windows.Forms.Label();
            this.optionsTabControl = new System.Windows.Forms.TabControl();
            this.justCloseButton = new System.Windows.Forms.Button();
            this.connectionOptionsTab.SuspendLayout();
            this.optionsTabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // saveAndCloseButton
            // 
            this.saveAndCloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveAndCloseButton.Location = new System.Drawing.Point(16, 224);
            this.saveAndCloseButton.Name = "saveAndCloseButton";
            this.saveAndCloseButton.Size = new System.Drawing.Size(121, 23);
            this.saveAndCloseButton.TabIndex = 1;
            this.saveAndCloseButton.Text = "Save and Close";
            this.saveAndCloseButton.UseVisualStyleBackColor = true;
            this.saveAndCloseButton.Click += new System.EventHandler(this.saveAndCloseButton_Click);
            // 
            // connectionOptionsTab
            // 
            this.connectionOptionsTab.Controls.Add(this.streamPortTextBox);
            this.connectionOptionsTab.Controls.Add(this.rcpPortTextBox);
            this.connectionOptionsTab.Controls.Add(this.ipAddressTextBox);
            this.connectionOptionsTab.Controls.Add(this.streamPortLabel);
            this.connectionOptionsTab.Controls.Add(this.rcpPortLabel);
            this.connectionOptionsTab.Controls.Add(this.ipAddressLabel);
            this.connectionOptionsTab.Location = new System.Drawing.Point(4, 25);
            this.connectionOptionsTab.Name = "connectionOptionsTab";
            this.connectionOptionsTab.Padding = new System.Windows.Forms.Padding(3);
            this.connectionOptionsTab.Size = new System.Drawing.Size(241, 181);
            this.connectionOptionsTab.TabIndex = 0;
            this.connectionOptionsTab.Text = "Connection";
            this.connectionOptionsTab.UseVisualStyleBackColor = true;
            // 
            // streamPortTextBox
            // 
            this.streamPortTextBox.Location = new System.Drawing.Point(7, 120);
            this.streamPortTextBox.Name = "streamPortTextBox";
            this.streamPortTextBox.Size = new System.Drawing.Size(100, 22);
            this.streamPortTextBox.TabIndex = 5;
            // 
            // rcpPortTextBox
            // 
            this.rcpPortTextBox.Location = new System.Drawing.Point(7, 74);
            this.rcpPortTextBox.Name = "rcpPortTextBox";
            this.rcpPortTextBox.Size = new System.Drawing.Size(100, 22);
            this.rcpPortTextBox.TabIndex = 3;
            // 
            // ipAddressTextBox
            // 
            this.ipAddressTextBox.Location = new System.Drawing.Point(7, 28);
            this.ipAddressTextBox.Name = "ipAddressTextBox";
            this.ipAddressTextBox.Size = new System.Drawing.Size(100, 22);
            this.ipAddressTextBox.TabIndex = 1;
            // 
            // streamPortLabel
            // 
            this.streamPortLabel.AutoSize = true;
            this.streamPortLabel.Location = new System.Drawing.Point(7, 99);
            this.streamPortLabel.Name = "streamPortLabel";
            this.streamPortLabel.Size = new System.Drawing.Size(83, 17);
            this.streamPortLabel.TabIndex = 4;
            this.streamPortLabel.Text = "Stream Port";
            // 
            // rcpPortLabel
            // 
            this.rcpPortLabel.AutoSize = true;
            this.rcpPortLabel.Location = new System.Drawing.Point(7, 53);
            this.rcpPortLabel.Name = "rcpPortLabel";
            this.rcpPortLabel.Size = new System.Drawing.Size(66, 17);
            this.rcpPortLabel.TabIndex = 2;
            this.rcpPortLabel.Text = "RCP Port";
            // 
            // ipAddressLabel
            // 
            this.ipAddressLabel.AutoSize = true;
            this.ipAddressLabel.Location = new System.Drawing.Point(7, 7);
            this.ipAddressLabel.Name = "ipAddressLabel";
            this.ipAddressLabel.Size = new System.Drawing.Size(76, 17);
            this.ipAddressLabel.TabIndex = 0;
            this.ipAddressLabel.Text = "IP Address";
            // 
            // optionsTabControl
            // 
            this.optionsTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.optionsTabControl.Controls.Add(this.connectionOptionsTab);
            this.optionsTabControl.Location = new System.Drawing.Point(12, 12);
            this.optionsTabControl.Name = "optionsTabControl";
            this.optionsTabControl.SelectedIndex = 0;
            this.optionsTabControl.Size = new System.Drawing.Size(249, 210);
            this.optionsTabControl.TabIndex = 0;
            // 
            // justCloseButton
            // 
            this.justCloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.justCloseButton.Location = new System.Drawing.Point(140, 224);
            this.justCloseButton.Name = "justCloseButton";
            this.justCloseButton.Size = new System.Drawing.Size(121, 23);
            this.justCloseButton.TabIndex = 2;
            this.justCloseButton.Text = "Just Close";
            this.justCloseButton.UseVisualStyleBackColor = true;
            this.justCloseButton.Click += new System.EventHandler(this.justCloseButton_Click);
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 257);
            this.ControlBox = false;
            this.Controls.Add(this.justCloseButton);
            this.Controls.Add(this.saveAndCloseButton);
            this.Controls.Add(this.optionsTabControl);
            this.Name = "OptionsForm";
            this.Text = "Options";
            this.connectionOptionsTab.ResumeLayout(false);
            this.connectionOptionsTab.PerformLayout();
            this.optionsTabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button saveAndCloseButton;
        private System.Windows.Forms.TabPage connectionOptionsTab;
        private System.Windows.Forms.TextBox streamPortTextBox;
        private System.Windows.Forms.TextBox rcpPortTextBox;
        private System.Windows.Forms.TextBox ipAddressTextBox;
        private System.Windows.Forms.Label streamPortLabel;
        private System.Windows.Forms.Label rcpPortLabel;
        private System.Windows.Forms.Label ipAddressLabel;
        private System.Windows.Forms.TabControl optionsTabControl;
        private System.Windows.Forms.Button justCloseButton;
    }
}